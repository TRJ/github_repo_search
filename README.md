
# Github Repo Fetch Application
This is an Flutter application that fetches Repositories from github REST API, Stores them 
locally for offline use. This application shows the Repository as a list, clicking on a 
particular item shows the details of the Repository

## Process Flow

 1.  On initialization, it tries to know the selected sort type. Sort Type can be either "updated" 
 or "star". Default is "updated".
 2.  After that, it starts fetching repositories 10 items per page.
 3.  Firstly, for a page, it tries to know if any record exists for a particular page and sort type.
 4.  If record exists locally, then it tries to know the last time when that particular page was 
    updated.
 5.  If the page was updated more that 30 seconds ago, then it fetches records for that page from 
    the api given there are valid internet connection.
 6.  Or else it shows the items from local database
 7.  If there is no record for a particular page or sort type, then it fetches from the Github API.

## UI
1. 
![Screenshot_20230119_200404.jpg](./assets/Screenshot_20230119_200404.jpg)
![Screenshot_20230119_200412.jpg](./assets/Screenshot_20230119_200412.jpg)

This image contains the list view of the fetched repository list. It has a two buttons in the 
appbar that will be used to control sort type selection. Each items of the list is clickable. By 
clicking, it will redirect to the details page of the Repository



2. ![Screenshot_20230119_200421.jpg](./assets/Screenshot_20230119_200421.jpg)

This image shows the details page for a repository. It contains Owner Name, Owner avatar, 
Description and last updated time.
