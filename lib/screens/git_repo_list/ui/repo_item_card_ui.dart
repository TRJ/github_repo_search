import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/constants/constant_colors.dart';
import 'package:github_repo_ferch/constants/constant_variables.dart';
import 'package:github_repo_ferch/extensions/widget_extention.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';
import 'package:github_repo_ferch/providers/repo_providers/providers.dart';
import 'package:sizer/sizer.dart';

import '../../../navigation/app_navigator.dart';

///[GithubRepoItemCardUI] is used for a showing a particular repo list
class GithubRepoItemCardUI extends StatelessWidget {
  const GithubRepoItemCardUI({Key? key, required this.repo}) : super(key: key);
  final GitHubRepoModel repo;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
      child: GestureDetector(
        onTap: () {
          AppNavigator.navigatorKey.currentState!.pushNamed(RouteNames.detailsScreen, arguments: repo);
        },
        // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.sp), color: primaryWhite,),
        child: Row(
          children: [
            Consumer(builder: (context, ref, _) {
              bool error = ref.watch(githubRepoAvatarUrlLoadErrorProvider(repo));
              return Container(
                height: 10.h,
                width: 10.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.sp),
                    image: DecorationImage(
                        image: !error
                            ? CachedNetworkImageProvider(
                                repo.avatarUrl,
                                errorListener: () {
                                  ref.read(githubRepoAvatarUrlLoadErrorProvider(repo).notifier).state = true;
                                },
                              )
                            : const AssetImage("assets/placeholder.jpg") as ImageProvider)),
              );
            }),
            Expanded(
                child: Container(
              padding: EdgeInsets.only(left: 10.sp),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    repo.name,
                    style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.bold, color: primaryBlue),
                  ),
                  1.sp.verticalSpace,
                  Text(
                    repo.fullName,
                    maxLines: 1,
                    style: TextStyle(overflow: TextOverflow.ellipsis, fontSize: 9.sp, color: black3),
                  ),
                  3.sp.verticalSpace,
                  repo.updatedDateTime != null
                      ? Text(
                          updatedDateTimeFormatToShow.format(repo.updatedDateTime!),
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 8.sp, color: primaryBlack),
                        )
                      : 1.verticalSpace
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
