
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/constants/constant_colors.dart';
import 'package:github_repo_ferch/providers/repo_providers/providers.dart';
import 'package:github_repo_ferch/util/sort_type_util.dart';
import 'package:sizer/sizer.dart';
///
/// [FilterChipUI] widget is used for showing filter option or Sort Option
class FilterChipUI extends ConsumerWidget {
  const FilterChipUI({Key? key, required this.sortType}) : super(key: key);
  final SortType sortType;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    SortType type = ref.watch(sortTypeProvider);
    return InkWell(
      onTap: (){
        if(type!=sortType){
          ref.read(sortTypeProvider.notifier).changeSortType(sortType);
        }
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(

          decoration: BoxDecoration(
              color: sortType==type?primaryWhite:secondaryPurple,
              borderRadius: BorderRadius.circular(15.sp)
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.sp,vertical: 5.sp),
            child: Text(sortType==SortType.updated?"Updated": "Star",style: TextStyle(color: sortType==type?primaryBlue:purple3,fontWeight: FontWeight.bold,fontSize: 10.sp),),
          ),
        ),
      ),
    );

  }
}
