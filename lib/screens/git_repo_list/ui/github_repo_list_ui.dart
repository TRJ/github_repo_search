import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/constants/constant_colors.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';
import 'package:github_repo_ferch/navigation/app_navigator.dart';
import 'package:github_repo_ferch/providers/repo_providers/providers.dart';
import 'package:github_repo_ferch/screens/git_repo_list/controllers/github_repo_controller.dart';
import 'package:github_repo_ferch/screens/git_repo_list/ui/filter_chip_ui.dart';
import 'package:github_repo_ferch/screens/git_repo_list/ui/repo_item_card_ui.dart';
import 'package:github_repo_ferch/util/sort_type_util.dart';
import 'package:sizer/sizer.dart';

/// [GithubRepoListUI] Widget show the list of repositories and a Sort Option for sorting the list;

class GithubRepoListUI extends ConsumerStatefulWidget {
  const GithubRepoListUI({Key? key}) : super(key: key);
  @override
  ConsumerState<GithubRepoListUI> createState() => _GithubRepoListUIState();
}

class _GithubRepoListUIState extends ConsumerState<GithubRepoListUI> {
  /// [_githubRepoController] is used when we need to connect functionality to the UI
  /// [_listViewScrollController] is used to controlling the scroll of github repo listview

  late final GithubRepoController _githubRepoController;
  late ScrollController _listViewScrollController;
  @override
  void initState() {
    super.initState();
    _githubRepoController = GithubRepoController(context: context, ref: ref);
    _listViewScrollController = ScrollController()..addListener(_loadMore);
  }

  void _loadMore(){
    bool loadMore = ref.read(githubRepoLoadMoreProvider);
    if(!loadMore && _listViewScrollController.offset >= _listViewScrollController.position.maxScrollExtent && !_listViewScrollController.position.outOfRange){
      ref.read(githubRepoListProvider.notifier).fetchMore();
    }
  }

  @override
  void dispose(){
    super.dispose();
    _listViewScrollController.removeListener(_loadMore);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Container(height: 14.h,width: 100.w, color: Colors.red,),
          Container(
            height: 14.h,
            width: 100.w,
            decoration: BoxDecoration(
                gradient: const LinearGradient(colors: [primaryPurple, primaryBlue], begin: Alignment.topLeft, end: Alignment.bottomRight),
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15.sp), bottomRight: Radius.circular(15.sp))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "Repositories",
                  style: TextStyle(color: primaryGrey, fontSize: 12.sp, fontWeight: FontWeight.bold),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [FilterChipUI(sortType: SortType.updated), FilterChipUI(sortType: SortType.stars)],
                )
              ],
            ),
          ),
          Expanded(child: Consumer(
            builder: (context, ref, _) {
              AsyncValue<List<GitHubRepoModel>> asyncRepos = ref.watch(githubRepoListProvider);
              return asyncRepos.when(
                  data: (repos) {
                    return ListView.builder(
                      controller: _listViewScrollController,
                      itemCount: repos.length,
                      itemBuilder: (context, i) {
                        GitHubRepoModel repo = repos[i];
                        return GithubRepoItemCardUI(repo: repo);
                      },
                    );
                  },
                  error: (e, s) => Container(),
                  loading: () => const Center(child: CircularProgressIndicator()));
            },
          )),
          Consumer(builder: (context, ref, _) {
            bool loadMore = ref.watch(githubRepoLoadMoreProvider);
            if (loadMore) {
              return const Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: CircularProgressIndicator(
                  color: primaryBlue,
                ),
              );
            } else {
              return const SizedBox(height: 1);
            }
          }),
         Consumer(
           builder: (context,ref,_){
             bool limitExceeded = ref.watch(githubRepoApiRateLimitExceededProvider);
             if(limitExceeded){
               WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                 const snackBar =SnackBar(
                   content:  Text('Github Repo Api Rate Limit Exceeded'),
                 );
                 ScaffoldMessenger.of(context).showSnackBar(snackBar);
               });
             }
             return Container();
           },
         )
        ],
      ),
    );
  }
}
