import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/extensions/widget_extention.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';
import 'package:sizer/sizer.dart';

import '../../../constants/constant_colors.dart';
import '../../../constants/constant_variables.dart';
import '../../../providers/repo_providers/providers.dart';

///[GithubRepoDetailsUI] widget used for showing Repository details
class GithubRepoDetailsUI extends StatelessWidget {
  const GithubRepoDetailsUI({Key? key, required this.repo}) : super(key: key);
  final GitHubRepoModel repo;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [

          /// Showing avatar image
          Positioned(
            top: 0,
            child: Consumer(
              builder: (context, ref, _) {
                bool error = ref.watch(githubRepoAvatarUrlLoadErrorProvider(repo));
                return Container(
                  width: 100.w,
                  height: 40.h,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image:!error
                              ? CachedNetworkImageProvider(repo.avatarUrl, errorListener: () {

                          }, maxHeight: 40.h.floor(), maxWidth: 100.w.floor()): AssetImage("assets/placeholder.jpg") as ImageProvider,
                          fit: BoxFit.fill)),
                );
              }
            ),
          ),

          //// details information
          Positioned(
            top: 4.h,
            left: 1.h,
            child: SizedBox(
              height: 6.h,
              width: 6.h,
              child: ElevatedButton(
                style: ButtonStyle(shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))),backgroundColor: MaterialStateProperty.all(primaryBlue)),
                onPressed: (){
                  Navigator.of(context).pop();
                },
                child:const Center(
                  child: Icon(Icons.arrow_back_ios,color: primaryWhite),
                ),
              ),
            ),
          ),
          Positioned(
            top: 35.h,

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left:10.w,),
                  child: Container(
                      width: 80.w,
                      decoration: BoxDecoration(
                        color: primaryGrey,
                        borderRadius: BorderRadius.circular(15.sp)
                      ),
                    padding: EdgeInsets.all(15.sp),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(repo.ownerLogin, style: TextStyle(fontSize: 14.sp, fontWeight: FontWeight.bold, color: primaryBlue),),
                        2.sp.verticalSpace,
                        Text(
                          repo.name,
                          style: TextStyle(fontSize: 11.sp, fontWeight: FontWeight.bold, color: primaryBlack),
                        ),
                        1.sp.verticalSpace,
                        Text(
                          repo.fullName,
                          style: TextStyle( fontSize: 9.sp, color: black3),
                        ),

                        1.sp.verticalSpace,
                        repo.updatedDateTime != null
                            ? Text(
                          updatedDateTimeFormatToShow.format(repo.updatedDateTime!),
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 8.sp, color: primaryBlack),
                        )
                            : 1.verticalSpace
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:  EdgeInsets.symmetric(horizontal: 5.w,vertical: 2.h),
                  child: SizedBox(
                    width: 80.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Description", style: TextStyle(fontSize: 14.5.sp, fontWeight: FontWeight.bold, color: primaryBlack)),
                        1.5.h.verticalSpace,
                        Text(repo.description,style: TextStyle(fontSize: 10.sp, color: black3),)
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
