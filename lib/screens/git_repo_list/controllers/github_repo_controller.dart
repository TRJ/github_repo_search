import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';

import '../../../navigation/app_navigator.dart';

/// [GithubRepoController] will contain UI related functionalities and connecting to services and other functionalities that clutter the UI //
/// [context] variable will have the current context so that we can maintain navigation, alerts from controller.
/// [ref] will be used to read/modify providers if needed in the contro
class GithubRepoController{
  final BuildContext context;
  final WidgetRef ref;
  GithubRepoController({required this.context, required this.ref});

}