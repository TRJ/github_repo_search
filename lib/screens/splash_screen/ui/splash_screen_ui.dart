import 'package:flutter/material.dart';
import 'package:github_repo_ferch/navigation/app_navigator.dart';
import 'package:sizer/sizer.dart';

import '../../../constants/constant_colors.dart';

class SpashScreenUI extends StatefulWidget {
  const SpashScreenUI({Key? key}) : super(key: key);

  @override
  State<SpashScreenUI> createState() => _SpashScreenUIState();
}

class _SpashScreenUIState extends State<SpashScreenUI> {
  @override
  void initState(){
    super.initState();
    _goto();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(gradient: LinearGradient(colors: [primaryPurple, primaryBlue], begin: Alignment.topLeft, end: Alignment.bottomRight)),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Text(
              "Please wait for a moment",
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12.sp),
            ),
            SizedBox(
              height: 2.h,
            ),
            const CircularProgressIndicator(
              color: Colors.white,
            ),
          ]),
        ),
      ),
    );
  }
  _goto()async{
    await Future.delayed(const Duration(seconds: 2));
    AppNavigator.navigatorKey.currentState!.pushNamedAndRemoveUntil(RouteNames.homeScreen, (route) => false);
  }
}
