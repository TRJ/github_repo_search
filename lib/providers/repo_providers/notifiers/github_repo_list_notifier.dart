import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/api/search_repo_api.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';
import 'package:github_repo_ferch/models/github_repo_response.dart';
import 'package:github_repo_ferch/providers/repo_providers/providers.dart';
import 'package:github_repo_ferch/services/db_services/github_repo_services.dart';
import 'package:github_repo_ferch/services/db_services/page_wise_update_time_services.dart';
import 'package:github_repo_ferch/services/db_tables/page_wise_update_time.dart';

import '../../../util/sort_type_util.dart';

///[GithubRepoListNotifier] is a StateNotifier for handling Github Repositories
///[ref] is used for accessing or modifying other providers
///[sortType] is the currently selected sortType
///
class GithubRepoListNotifier extends StateNotifier<AsyncValue<List<GitHubRepoModel>>> {
  final Ref ref;
  final SortType sortType;
  GithubRepoListNotifier(this.ref, this.sortType) : super(const AsyncLoading()) {
    _init();
  }

  final PageWiseUpdateTimeServices _pageWiseUpdateTimeServices = PageWiseUpdateTimeServices();
  final GithubRepoServices _githubRepoServices = GithubRepoServices();

  ///[perPage] indicates how many records to fetch per page
  ///[pageNo] indicates current page no. it will increase if we fetch more pages
  ///[keyword] is the required keyword from which we will search our repository
  ///[total] contains the total number of repository exists for a sortType
  ///[repos] contains all the repos that have been fetched till now
  int perPage = 10;
  int pageNo = 1;
  String keyword = "Flutter";
  int total = 0;
  List<GitHubRepoModel> repos = [];

  //initial data fetch when provider watch begin
  _init() async {
    repos = [];
    pageNo = 1;
    ref.refresh(githubRepoLoadMoreProvider);
    await fetchForASpecificPageAndSortType();
  }

  ///[fetchForASpecificPageAndSortType] fetches repository either from local or API for a specific [pageNo] and [sortType]
  fetchForASpecificPageAndSortType() async {
    bool needUpdating = await _pageWiseUpdateTimeServices.checkIfPageNeedsUpdating(pageNo, sortType);
    List<GitHubRepoModel> newRepos = [];
    if (needUpdating) {
      GitHubRepoResponse response = await SearchRepoApi().fetch(keyword: keyword, sortType: sortType, pageNo: pageNo, perPage: perPage);
      total = response.total;
      newRepos = response.repos;
      handleRateLimitExceedForAResponse(response);
      if (newRepos.isNotEmpty) {
        await _githubRepoServices.insertNewGithubReposForASpecificPageAndSortType(response.repos, pageNo, sortType);
        await _pageWiseUpdateTimeServices.insertOrUpdateAPageUpdateTime(pageNo, sortType);
      }
    } else {
      GitHubRepoResponse response = await _githubRepoServices.fetchRepoFromLocal(pageNo, sortType);
      newRepos = response.repos;
    }
    if (newRepos.isNotEmpty) {
      repos.addAll(newRepos);
      state = AsyncData([...repos]);
      pageNo++;
    } else {
      if(pageNo==1){
        state =const AsyncData([]);
      }
    }
  }

  ///[handleRateLimitExceedForAResponse] handles state management functionality when api call limit exceeds
  handleRateLimitExceedForAResponse(GitHubRepoResponse response) {
    if (response.rateLimitExceeded) {
      ref.refresh(githubRepoApiRateLimitExceededProvider.notifier).state = true;
      if (pageNo == 1) {
        state = const AsyncData([]);
      }
    }
  }

  ///[fetchMore] is used to fetch more repos after initial fetch
  fetchMore() async {
    ref.read(githubRepoLoadMoreProvider.notifier).state = true;
    await fetchForASpecificPageAndSortType();
    ref.read(githubRepoLoadMoreProvider.notifier).state = false;
  }
}
