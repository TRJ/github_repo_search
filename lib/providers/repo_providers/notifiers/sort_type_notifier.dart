import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/services/db_services/sort_type_services.dart';
import 'package:github_repo_ferch/util/sort_type_util.dart';

///[SortTypeNotifier] is a stateNotifier for storing selected sortType
class SortTypeNotifier extends StateNotifier<SortType> {
  SortTypeNotifier() : super(SortType.updated) {
    _init();
  }

  _init() async {
    SortType sortType = await SortTypeServices().fetchSelectedSortType();
    state = sortType;
  }

  ///[changeSortType] used for changing currently selected sort type
  changeSortType(SortType sortType) async {
    state = sortType;
    await SortTypeServices().insertOrUpdateSortType(sortType);
  }
}
