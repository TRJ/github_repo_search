import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:github_repo_ferch/providers/repo_providers/notifiers/github_repo_list_notifier.dart';
import 'package:github_repo_ferch/providers/repo_providers/notifiers/sort_type_notifier.dart';
import 'package:github_repo_ferch/util/sort_type_util.dart';

import '../../models/github_repo_model.dart';

///[sortTypeProvider] provider that will store the currently selected sort Type
final sortTypeProvider = StateNotifierProvider<SortTypeNotifier, SortType>((ref) => SortTypeNotifier());

///[githubRepoListProvider] provider will store currently fetched github repo list
final githubRepoListProvider = StateNotifierProvider.autoDispose<GithubRepoListNotifier, AsyncValue<List<GitHubRepoModel>>>((ref) {
  SortType sortType = ref.watch(sortTypeProvider);
  return GithubRepoListNotifier(ref, sortType);
});

///[githubRepoLoadMoreProvider] provider indicates if more items are being loaded
final githubRepoLoadMoreProvider = StateProvider.autoDispose<bool>((ref) => false);

///[githubRepoAvatarUrlLoadErrorProvider] provider indicates if error occured fetching avater image
final githubRepoAvatarUrlLoadErrorProvider = StateProvider.family.autoDispose<bool, GitHubRepoModel>((ref, repo) => false);

/// [githubRepoApiRateLimitExceededProvider] provider indicates if github REST API call limit exceeded
final githubRepoApiRateLimitExceededProvider = StateProvider.autoDispose((ref) => false);
