import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:github_repo_ferch/constants/constant_variables.dart';

///[GitHubRepoModel] represents a single repo
class GitHubRepoModel {
  final int repoId;
  final String nodeId;
  final String name;
  final String fullName;
  final String ownerLogin;
  final String avatarUrl;
  final String description;
  final DateTime? updatedDateTime;

  GitHubRepoModel(
      {required this.repoId,
      required this.nodeId,
      required this.name,
      required this.fullName,
      required this.ownerLogin,
      required this.avatarUrl,
      required this.description,
      required this.updatedDateTime});

  ///[fromJson] factory constructor is used to make a repo object from Map got from api
  factory GitHubRepoModel.fromJson(Map json) {
    DateTime? updatedDateTimeNullable;
    if (json.containsKey("updated_at")) {
      try {
        updatedDateTimeNullable = DateTime.tryParse(json["updated_at"]);
      } catch (e) {
        debugPrint("inside github repo model fromjson catch block $e");
      }
    }
    return GitHubRepoModel(
        repoId: json["id"],
        nodeId: json["node_id"] ?? "N/A",
        name: json["name"] ?? "N/A",
        fullName: json["full_name"] ?? "N/A",
        ownerLogin: json["owner"]["login"] ?? "N/A",
        avatarUrl: json["owner"]["avatar_url"] ?? "N/A",
        description: json["description"] ?? "N/A",
        updatedDateTime: updatedDateTimeNullable);
  }


  ///[fromLocal] factory constructor is used to make a repo object from Map got from offline sqlite DB
  factory GitHubRepoModel.fromLocal(Map local) {
    return GitHubRepoModel(
        repoId: local["repo_id"],
        nodeId: local["node_id"],
        name: local["name"],
        fullName: local["full_name"],
        ownerLogin: local["owner_login"],
        avatarUrl: local["avatar_url"],
        description: local["description"],
        updatedDateTime: local["updated_at"] != null ? DateTime.tryParse(local["updated_at"]) : null);
  }

  /// [toJson] function returns Map containing all the necessary information for a Repo to store it easily in the sqlite DB
  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "repo_id": repoId,
      "node_id": nodeId,
      "name": name,
      "full_name": fullName,
      "owner_login": ownerLogin,
      "avatar_url": avatarUrl,
      "description": description,
      "updated_at": updatedDateTime != null ? apiDateTimeFormat.format(updatedDateTime!) : null
    };
  }
}
