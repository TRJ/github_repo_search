import 'github_repo_model.dart';

/// [GitHubRepoResponse] class is used to cary response from api call or local DB call to notifier
/// [total] indicates total number of repos for a particular [SortType]
/// [repos] contains list of repo currently fetched
/// [rateLimitExceeded] denotes if maximum allowable limit exceeded for Github REST API
class GitHubRepoResponse{
  final int total;
  final List<GitHubRepoModel> repos;
  final bool rateLimitExceeded;
  GitHubRepoResponse({required this.total, required this.repos, this.rateLimitExceeded = false});
}