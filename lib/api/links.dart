///[Links] contains all the api links for the project
class Links{

  ///[baseUrl] is the base url of the api
  static const String baseUrl = "https://api.github.com";

  ///[searchRepoUrl] used for making api call to search repos;
  static const String searchRepoUrl = "/search/repositories";
}
