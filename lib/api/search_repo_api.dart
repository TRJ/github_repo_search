import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:github_repo_ferch/api/links.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';
import 'package:github_repo_ferch/models/github_repo_response.dart';
import 'package:github_repo_ferch/util/sort_type_util.dart';
import 'package:http/http.dart' as http;

/// [SearchRepoApi] class contains function for the api call
class SearchRepoApi{

  /// [fetch] function calls the api to fetch repository from the api
  ///
  Future<GitHubRepoResponse> fetch({required String keyword, required SortType sortType, required int pageNo, required int perPage,})async{
    List<GitHubRepoModel> repos = [];
    int total = 0;
    bool rateLimitExceeded = false;
    try{
      String sort= SortTypeUtil.stringFromSortType(sortType);
      String urlString = "${Links.baseUrl}${Links.searchRepoUrl}?q=$keyword&sort=$sort&per_page=$perPage&page=$pageNo";
      Uri url = Uri.parse(urlString);

      ///Call API. API will hit time out at 10 seconds
      var response = await http.get(url,headers: {"accept":"application/vnd.github+json"}).timeout(const Duration(seconds: 10));
      if(response.statusCode==200){
        Map info = jsonDecode(response.body);
        total = info["total_count"]??0;
        List items = info["items"]??[];
        if(items.isNotEmpty){
          for(Map item in items){
            GitHubRepoModel repo = GitHubRepoModel.fromJson(item);
            repos.add(repo);
          }
        }
      }else if(response.statusCode==403){
        rateLimitExceeded = true;
      }
    }catch(e){
      debugPrint("inside fetch SearchRepoAPi catch block $e");
    }
    return GitHubRepoResponse(total: total, repos: repos, rateLimitExceeded: rateLimitExceeded);
  }

}