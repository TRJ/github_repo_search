import 'package:flutter/foundation.dart';
import 'package:github_repo_ferch/constants/constant_variables.dart';
import 'package:github_repo_ferch/services/db_services/db_creator.dart';
import 'package:internet_popup/internet_popup.dart';
import 'package:ntp/ntp.dart';
import 'package:sqflite/sqflite.dart';

import '../../util/sort_type_util.dart';
import '../db_tables/page_wise_update_time.dart';

///[PageWiseUpdateTimeServices] contains functions to maintain if a page for a particular sort type needs updating
class PageWiseUpdateTimeServices extends DBCreator {
  /// [checkIfPageNeedsUpdating] checks if a page needs to be refreshed from api
  Future<bool> checkIfPageNeedsUpdating(int pageNo, SortType sortType) async {
    bool needUpdate = false;
    try {
      bool internet = await InternetPopup().checkInternet();
      if (internet) {
        DateTime? updatedAt = await getLastUpdatedDatetimeForAPage(pageNo, sortType);
        if (updatedAt != null) {
          DateTime currentTime = await NTP.now();
          Duration difference = currentTime.difference(updatedAt);
          if (difference.inMinutes > 30) {
            needUpdate = true;
          } else {
            needUpdate = false;
          }
        } else {
          needUpdate = true;
        }
      }
    } catch (e) {
      debugPrint("inside checkIfPageNeedsUpdating githubRepoServices catch block $e");
    }
    return needUpdate;
  }

  /// [getLastUpdatedDatetimeForAPage] returns [DateTime] when a [pageNo] last updated
  Future<DateTime?> getLastUpdatedDatetimeForAPage(int pageNo, SortType sortType) async {
    DateTime? updatedAt;
    try {
      Map<String, dynamic> updateTime = await fetchASpecificPage(pageNo, sortType);
      if (updateTime.containsKey(PageWiseUpdateTime.updatedAt)) {
        if (updateTime[PageWiseUpdateTime.updatedAt] != null) {
          updatedAt = DateTime.tryParse(updateTime[PageWiseUpdateTime.updatedAt]);
        }
      }
    } catch (e) {
      debugPrint("inside getLastUpdatedDatetimeForAPage githubRepoServices catch block $e");
    }
    return updatedAt;
  }

  ///[fetchASpecificPage] queries update info for a particular [pageNo] and [sortType]
  Future<Map<String, dynamic>> fetchASpecificPage(int pageNo, SortType sortType) async {
    Map<String, dynamic> pageWiseUpdateTime = {};
    try {
      Database db = await getDatabase();
      String q = '''SELECT * FROM ${PageWiseUpdateTime.tableName} WHERE ${PageWiseUpdateTime.pageNo}=? AND ${PageWiseUpdateTime.sortType}=?''';
      List<dynamic> params = [pageNo, SortTypeUtil.stringFromSortType(sortType)];
      List<Map<String, dynamic>> pageWiseUpdateTimeList = await db.rawQuery(q, params);
      if (pageWiseUpdateTimeList.isNotEmpty) {
        pageWiseUpdateTime = pageWiseUpdateTimeList[0];
      }
    } catch (e) {
      debugPrint("inside fetchASpecificPage GithubRepoServices catch block $e");
    }
    return pageWiseUpdateTime;
  }

  ///[insertOrUpdateAPageUpdateTime] inserts of updates last updated time for a [pageNo] and [sortType]
  Future<void> insertOrUpdateAPageUpdateTime(int pageNo, SortType sortType) async {
    try {
      Map<String, dynamic> updateTime = await fetchASpecificPage(pageNo, sortType);
      Database db = await getDatabase();
      String currentTime = apiDateTimeFormat.format(await NTP.now());
      if (updateTime.isEmpty) {
        updateTime = {PageWiseUpdateTime.pageNo: pageNo, PageWiseUpdateTime.sortType: SortTypeUtil.stringFromSortType(sortType), PageWiseUpdateTime.updatedAt: currentTime};
        await db.insert(PageWiseUpdateTime.tableName, updateTime);
      } else {
        await db.update(PageWiseUpdateTime.tableName, {PageWiseUpdateTime.updatedAt: currentTime},
            where: '${PageWiseUpdateTime.pageNo}=? AND ${PageWiseUpdateTime.sortType}=?', whereArgs: [pageNo, SortTypeUtil.stringFromSortType(sortType)]);
      }
    } catch (e) {
      debugPrint("inside insertOrUpdateAPageUpdateTime githubRepoServices catch block $e");
    }
  }
}
