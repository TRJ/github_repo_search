import 'dart:io';

import 'package:github_repo_ferch/providers/repo_providers/providers.dart';
import 'package:github_repo_ferch/services/db_tables/github_repo_table.dart';
import 'package:github_repo_ferch/services/db_tables/page_wise_update_time.dart';
import 'package:github_repo_ferch/services/db_tables/sort_type_table.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
///[DBCreator] class creates github_repo_db in a specific directory if not created earlier
///

class DBCreator{
  Database? db;

  ///[getDatabase] function returns Database object
  Future<Database> getDatabase()async{
   return db??await makeDB();
  }

  /// [makeDB] returns a Database object. creates db and necessary tables if not created
  Future<Database> makeDB()async{
    final path = await getDatabasePath('github_repo_db');
    Database db =await openDatabase(path, version: 1, onCreate: onCreate);
    return db;
  }

 /// [onCreate]  creates necessary tables for the db
 Future<void> onCreate(Database db, int version)async{
    await GithubRepoTable.createTable(db);
    await PageWiseUpdateTime.createTable(db);
    await SortTypeTable.createTable(db);
  }

  ///[getDatabasePath] returns DB path. creates directory if not created
  Future<String> getDatabasePath(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);
    bool dbExists =await Directory(dirname(path)).exists();
    //make sure the folder exists
    if (!dbExists) {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }
}