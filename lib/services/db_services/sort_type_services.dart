import 'package:flutter/cupertino.dart';
import 'package:github_repo_ferch/services/db_services/db_creator.dart';
import 'package:github_repo_ferch/services/db_tables/sort_type_table.dart';
import 'package:github_repo_ferch/util/sort_type_util.dart';
import 'package:sqflite/sqflite.dart';
/// [SortTypeServices] contains functions maintain storing of currently selected sort type

class SortTypeServices extends DBCreator{

  ///[fetchSelectedSortType] used to get the currently selected sortType
  Future<SortType> fetchSelectedSortType()async{
    SortType sortType = SortType.updated;
    try{
      Map<String,dynamic> sortTypeMap = await fetchSortType();
      if(sortTypeMap.containsKey(SortTypeTable.sortType)){
        sortType = SortTypeUtil.sortTypeFromString(sortTypeMap[SortTypeTable.sortType]??"");
      }
    }catch(e){
      debugPrint("inside fetchSelectedSortType sortTypeServices catch block $e");
    }
    return sortType;
  }

  ///[fetchSortType] queries sort type in the table to fetch the currently selected sort type
  Future<Map<String,dynamic>> fetchSortType()async{
    Map<String,dynamic> sortTypeMap = {};
    try{
      Database db = await getDatabase();
     List sortTypeList = await db.query(SortTypeTable.tableName,limit: 1);
     if(sortTypeList.isNotEmpty){
       sortTypeMap = sortTypeList[0];
     }
    }catch(e){
      debugPrint("inside fetchSortType SortTypeServices catch block $e");
    }
    return sortTypeMap;
  }

  ///[insertOrUpdateSortType] stores currently selected sort Type
  Future<void> insertOrUpdateSortType(SortType type)async{
    try{
      Map<String,dynamic> sortType = await fetchSortType();
      String sortTypeString = SortTypeUtil.stringFromSortType(type);
      Database db = await getDatabase();
      if(sortType.isNotEmpty){
        await db.update(SortTypeTable.tableName, {SortTypeTable.sortType:sortTypeString});
      }else{
        await db.insert(SortTypeTable.tableName, {SortTypeTable.sortType:sortTypeString});
      }
    }catch(e){
      debugPrint("inside insertOrUpdateSortType SortTypeServices catch block $e");
    }
  }
}