import 'package:flutter/cupertino.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';
import 'package:github_repo_ferch/models/github_repo_response.dart';
import 'package:github_repo_ferch/services/db_services/db_creator.dart';
import 'package:github_repo_ferch/services/db_tables/github_repo_table.dart';
import 'package:github_repo_ferch/services/db_tables/page_wise_update_time.dart';
import 'package:github_repo_ferch/util/sort_type_util.dart';
import 'package:internet_popup/internet_popup.dart';
import 'package:ntp/ntp.dart';
import 'package:sqflite/sqflite.dart';


///[GithubRepoServices] contains functions related to storing and fetching github repo information to and from Sqlite DB
class GithubRepoServices extends DBCreator {

  /// [fetchRepoFromLocal] fetches all the repository for a particular [pageNo] and [sortType]
  Future<GitHubRepoResponse> fetchRepoFromLocal(int pageNo,SortType sortType) async {
    Database db = await getDatabase();
    List<GitHubRepoModel> repos = [];
    int total = 0;
    try {
      String selectQuery = '''SELECT * FROM ${GithubRepoTable.tableName} WHERE ${GithubRepoTable.pageNo}=? AND ${GithubRepoTable.sortType}=?''';
      List<dynamic> params = [pageNo,SortTypeUtil.stringFromSortType(sortType)];
      List<Map<String, dynamic>> repoList = await db.rawQuery(selectQuery, params);
      if (repoList.isNotEmpty) {
        for (Map<String, dynamic> repoMap in repoList) {
          GitHubRepoModel repo = GitHubRepoModel.fromLocal(repoMap);
          repos.add(repo);
        }
      }
    } catch (e) {
      debugPrint("inside fetchRepoFromLocal GithubRepoServices catch block $e");
    }

    return GitHubRepoResponse(total: total, repos: repos);
  }

  /// [insertNewGithubReposForASpecificPageAndSortType] function inserts [repos] for a particular [pageNo] and [sortType]
  Future<void> insertNewGithubReposForASpecificPageAndSortType(List<GitHubRepoModel> repos, int pageNo, SortType sortType)async{
    try{
      Database db = await getDatabase();
      Batch batch = db.batch();
      await deleteAllItemForASpecificPageAndSortType(pageNo, sortType);
      if(repos.isNotEmpty){
        for(GitHubRepoModel repo in repos){
          Map<String,dynamic> row = repo.toJson();
          row[GithubRepoTable.pageNo] = pageNo;
          row[GithubRepoTable.sortType] = SortTypeUtil.stringFromSortType(sortType);
          batch.insert(GithubRepoTable.tableName, row);
        }
        await batch.commit(noResult: true);
      }
    }catch(e){
      debugPrint("inside insertNewGithubReposForASpecificPageAndSortType GithubRepoServices catch block $e");
    }
  }

  /// [deleteAllItemForASpecificPageAndSortType] deletes all the record for a particular [pageNo] and [sortType]
  Future<void> deleteAllItemForASpecificPageAndSortType(int pageNo, SortType sortType)async{
    try{
      Database db =await getDatabase();
      await db.delete(GithubRepoTable.tableName,where: "${GithubRepoTable.pageNo}=? AND ${GithubRepoTable.sortType}=?",whereArgs: [pageNo,SortTypeUtil.stringFromSortType(sortType)]);
    }catch(e){
      debugPrint("inside deleteAllItemForASpecificPageAndSortType GithubRepoServices catch block $e");
    }
  }


}
