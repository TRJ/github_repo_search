import 'package:sqflite/sqflite.dart';

/// [GithubRepoTable] has table information
/// [tableName] contains Name of the table
/// [id] is the primary Key of the table
/// [pageNo] denotes page number for the data
/// [sortType] denotes sort type for the data
/// [createTable] function creates github_repo_table
class GithubRepoTable {
  static const tableName = "github_repo_table";

  // table columns
  static const id = "id";
  static const repoId = "repo_id";
  static const nodeId = "node_id";
  static const pageNo = "page_no";
  static const name = "name";
  static const fullName = "full_name";
  static const ownerLogin = "owner_login";
  static const avatarUrl = "avatar_url";
  static const description = "description";
  static const updatedAt = "updated_at";
  static const sortType = "sort_type";


  static createTable(Database db) async{
    const String createSql = '''CREATE TABLE $tableName
    (
    $id INTEGER PRIMARY KEY AUTOINCREMENT,
    $repoId INTEGER,
    $nodeId TEXT,
    $pageNo INTEGER,
    $name TEXT,
    $fullName TEXT,
    $ownerLogin TEXT,
    $avatarUrl TEXT,
    $description TEXT,
    $sortType TEXT,
    $updatedAt TEXT NULL
    )
    ''';
   await db.execute(createSql);
  }
}
