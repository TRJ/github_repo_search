import 'package:sqflite/sqflite.dart';

/// [SortTypeTable] has table information
/// [tableName] contains Name of the table
/// stores currently selected sort Type
/// [createTable] unction creates sort_type_table
class SortTypeTable {
  static const tableName = "sort_type_table";

  static const id = "id";
  static const sortType = "sort_type";

  static createTable(Database db) async {
    const createSql = '''CREATE TABLE $tableName
     (
     $id INTEGER PRIMARY KEY AUTOINCREMENT,
     $sortType TEXT
     )
     ''';
    await db.execute(createSql);
  }
}
