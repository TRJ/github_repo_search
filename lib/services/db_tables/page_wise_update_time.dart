import 'package:sqflite/sqflite.dart';

/// [PageWiseUpdateTime] has table information
/// [tableName] contains Name of the table
/// [updatedAt] column denotes the last update time for [sortType] and [pageNo]
/// [createTable] unction creates page_wise_update_time
class PageWiseUpdateTime {
  static const tableName = "page_wise_update_time";

  static const id = "id";
  static const pageNo = "page_no";
  static const sortType = "sort_type";
  static const updatedAt = "updated_at";

  static createTable(Database db) async {
    const createSql = '''CREATE TABLE $tableName
     (
     $id INTEGER PRIMARY KEY AUTOINCREMENT,
     $pageNo INTEGER,
     $sortType TEXT,
     $updatedAt TEXT
     )
     ''';
    await db.execute(createSql);
  }
}
