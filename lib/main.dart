import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:github_repo_ferch/screens/git_repo_list/ui/github_repo_list_ui.dart';
import 'package:sizer/sizer.dart';
import 'constants/constant_colors.dart';
import 'navigation/app_navigator.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(
      SystemUiMode.manual,
      overlays: [SystemUiOverlay.top, SystemUiOverlay.bottom],
    );

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp(
        title: 'Github Repo Search',
        theme: ThemeData(
          fontFamily: 'Inter',
          primaryColor: primaryBlue,
          scaffoldBackgroundColor: primaryWhite,
          splashColor: primaryBlue.withOpacity(.5),
          highlightColor: primaryBlue.withOpacity(.3),
        ),
        onGenerateRoute: AppNavigator.generateRoutes,
        scaffoldMessengerKey: AppNavigator.scaffoldMessengerKey,
        navigatorKey: AppNavigator.navigatorKey,
      );
    });
  }
}
