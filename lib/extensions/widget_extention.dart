

import 'package:flutter/material.dart';

///[WidgetExtension] used for easy access to vertical and horizontal spaces
extension WidgetExtension on num {
  SizedBox get horizontalSpace => SizedBox(width: toDouble());
  SizedBox get verticalSpace => SizedBox(height: toDouble());
}
