import 'package:flutter/material.dart';

const Color primaryBlue = Color(0xFF5D5FC1);
const primaryGrey = Color(0xFFEBE8E8);
const Color primaryPurple = Color(0xFFAA86DD);
const Color primaryWhite=  Color(0xFFFFFEFF);
const Color secondaryPurple = Color(0xFF6D66C7);
const Color purple3 = Color(0xFFA098E2);
const Color primaryBlack = Color(0xFF121212);
const Color black2 = Color(0xFFB9B9B9);
const Color black3 = Color(0xFF7C7C7C);
