import 'package:flutter/material.dart';
import 'package:github_repo_ferch/models/github_repo_model.dart';
import 'package:github_repo_ferch/screens/git_repo_list/ui/github_repo_details_ui.dart';
import 'package:github_repo_ferch/screens/git_repo_list/ui/github_repo_list_ui.dart';

import '../screens/splash_screen/ui/splash_screen_ui.dart';


///[RouteNames] contains all the route names exists in the application
///[splashScreen] contains routeName for splash screen
///[homeScreen] contains routeName for Github Repo List Page
///[detailsScreen] contains routeName for Github Repo Details
class RouteNames {
  RouteNames._();
  static const splashScreen = '/splash-screen';
  static const homeScreen = '/home-screen';
  static const detailsScreen = '/details_screen';
}

///[AppNavigator] class is used for navigation
class AppNavigator {
  AppNavigator._();

  ///[navigatorKey] is used for navigation, contains current state for navigation
  static final navigatorKey = GlobalKey<NavigatorState>();
  static final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  ///[generateRoutes] function return a [MaterialPageRoute] for app navigation
  static Route<dynamic> generateRoutes(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) {
        return getScreenByName(settings.name, settings.arguments);
      },
    );
  }

  ///[getScreenByName] returns a material route for a specific page
  static dynamic getScreenByName(String? name, Object? arguments) {
    switch (name) {
      case RouteNames.homeScreen:
        return const GithubRepoListUI();
      case RouteNames.detailsScreen:
        return  GithubRepoDetailsUI(repo: arguments as GitHubRepoModel);
      case RouteNames.splashScreen:
        return const SpashScreenUI();
      default:
        return const SpashScreenUI();
    }
  }
}
