/// [SortType] defines the the types of sort features that are enabled
enum SortType { updated, stars }

/// [SortTypeUtil] class is used for converting to and from [String] to [SortType]
class SortTypeUtil {

  /// [stringFromSortType] function returns [String] from [SortType]
  static String stringFromSortType(SortType sortType) {
    switch (sortType) {
      case SortType.updated:
        return "updated";
      case SortType.stars:
        return "stars";
      default:
        return "updated";
    }
  }

  ///[sortTypeFromString] function returns [SortType] from [String]
  static SortType sortTypeFromString(String sort) {
    switch (sort) {
      case "updated":
        return SortType.updated;
      case "stars":
        return SortType.stars;
      default:
        return SortType.updated;
    }
  }
}
